package fr.scholanova.eial.archdist.PresentationEjbInterface;

import java.io.Serializable;

public class Repas implements Serializable{
	
	private String nom;
	private double prix;
	private boolean dehors;
	
	public Repas(String string, double d, boolean b) {
		// TODO Auto-generated constructor stub
		
		nom = string;
		prix = d;
		dehors = b;
	}

	@Override
	public String toString() {
		return "Repas [nom=" + nom + ", prix=" + prix + ", dehors=" + dehors + "]";
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public boolean isDehors() {
		return dehors;
	}

	public void setDehors(boolean dehors) {
		this.dehors = dehors;
	}
}
